﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaLogica
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Introduce el número de casos de prueba: ");
                var length = int.Parse(Console.ReadLine());
                var cases = new int[length, 2];

                for (var i = 0; i < length; i++)
                {
                    Console.Write($"\nCaso #{(i + 1)}\n Introduce las variables N y M (Números separados por un espacio): ");
                    var line = Console.ReadLine();
                    var result = line.Split(' ').Select(x => int.Parse(x)).ToArray();
                    cases[i, 0] = result[0];
                    cases[i, 1] = result[1];
                }

                var results = new List<char>();
                for (var i = 0; i < length; i++)
                {
                    Console.Write($"\nResultado caso de prueba #{(i + 1)}:\n");
                    int N = cases[i, 0];
                    int M = cases[i, 1];
                    if (N <= M)
                    {
                        if (N % 2 != 0)
                        {
                            Console.WriteLine("R");
                        }
                        else
                        {
                            Console.WriteLine("L");
                        }
                    }
                    else
                    {
                        if (M % 2 != 0)
                        {
                            Console.WriteLine("D");
                        }
                        else
                        {
                            Console.WriteLine("U");
                        }
                    }
                }
                Console.ReadKey();
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
