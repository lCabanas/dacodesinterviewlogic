### Prueba Lógica 

- Para poder compilar el proyecto es necesario tener Instalado Visual Studio 2017 Se puede obtener del sitio https://visualstudio.microsoft.com/vs/community/

- En la instalación es necesario seleccionar todos los componentes relacionados al lenguaje C#.

- Después de instalar ya se podrá abrir la solución del proyecto, basta con compilar y presionar el botón ejecutar para correr el proyecto.

- Al iniciar la aplicación en consola se pedirá inicialmente al usuario el número de casos de prueba
- Luego se solicitara los valores de cada caso de prueba hasta completar el número total ingresado anteriormente.
